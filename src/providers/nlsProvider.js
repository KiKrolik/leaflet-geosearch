import BaseProvider from './provider';

export default class Provider extends BaseProvider {
  endpoint({ query } = {}) {
    const { params } = this.options;

    const paramString = this.getParamString({
      ...params,
      text: query,
    });

    // requires a secure connection
    return `https://avoin-paikkatieto.maanmittauslaitos.fi/geocoding/v1/pelias/search?${paramString}`;
  }

  endpointReverse({ point } = {}) {
    const { params } = this.options;

    const paramString = this.getParamString({
      ...params,
      'point.lat': point.lat,
      'point.lon': point.lon,
    });

    return `https://avoin-paikkatieto.maanmittauslaitos.fi/geocoding/v1/pelias/reverse?${paramString}`;
  }

  parse({ data }) {
    return data.features.map((f) => {
      const layer = L.geoJSON(f.geometry);
      const bounds = layer.getBounds();
      const center = bounds.getCenter();
      const southwest = bounds.getSouthWest();
      const northeast = bounds.getNorthEast();
      return {
        x: center.lng,
        y: center.lat,
        label: f.properties.label,
        bounds: [
          [southwest.lat, southwest.lng], // s, w
          [northeast.lat, northeast.lng], // n, e
        ],
        raw: f,
      };
    });
  }

  async search({ query, point }) {
    const url = point
      ? this.endpointReverse({ point })
      : this.endpoint({ query });

    const request = await fetch(url);
    const json = await request.json();
    return this.parse({ data: point ? [json] : json });
  }
}
